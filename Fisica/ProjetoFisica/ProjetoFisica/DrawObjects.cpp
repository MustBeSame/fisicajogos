#include "DrawObjects.h"



DrawObjects::DrawObjects()
{
}


DrawObjects::~DrawObjects()
{
}

void DrawObjects::CreateGround(b2World & World, float X, float Y)
{
	//b2BodyDef BodyDef;
	//BodyDef.position = b2Vec2(X / 30.f, Y / 30.f);
	//BodyDef.type = b2_staticBody;
	//b2Body* Body = World.CreateBody(&BodyDef);

	//b2PolygonShape Shape;
	//b2Vec2* points = new b2Vec2(50.f, 90.f);
	//Shape.Set(points, 4); 
	//b2FixtureDef FixtureDef;
	//FixtureDef.density = 0.f;  // Sets the density of the body
	//FixtureDef.shape = &Shape; // Sets the shape
	//Body->CreateFixture(&FixtureDef); // Apply the fixture definition
}

void DrawObjects::CreateLine(b2World & World, int MouseX, int MouseY, float heigth, float width, float scale)
{
	b2BodyDef BodyDef;
	BodyDef.position = b2Vec2(MouseX / scale, MouseY / scale);
	BodyDef.userData = "Line";
	BodyDef.type = b2_staticBody;
	b2Body* Body = World.CreateBody(&BodyDef);

	b2PolygonShape Shape;
	Shape.SetAsBox((width / 2) / scale, (heigth / 2) / scale);
	b2FixtureDef FixtureDef;
	FixtureDef.density = 1.f;
	FixtureDef.friction = 0.7f;
	FixtureDef.shape = &Shape;
	Body->CreateFixture(&FixtureDef);
}

void DrawObjects::CreateBox(b2World & World, int MouseX, int MouseY, float heigth, float width, float scale)
{
	b2BodyDef BodyDef;
	BodyDef.position = b2Vec2(MouseX / scale, MouseY / scale);
	BodyDef.type = b2_dynamicBody;
	b2Body* Body = World.CreateBody(&BodyDef);

	b2PolygonShape Shape;
	Shape.SetAsBox((width / 2) / scale, (heigth / 2) / scale);
	b2FixtureDef FixtureDef;
	FixtureDef.density = 1.f;
	FixtureDef.friction = 0.7f;
	FixtureDef.shape = &Shape;
	Body->CreateFixture(&FixtureDef);
}

void DrawObjects::CreateCircle(b2World &World, float radius, int MouseX, int MouseY, float scale)
{
	b2BodyDef BodyDef;
	BodyDef.position = b2Vec2(MouseX / scale , MouseY / scale);
	BodyDef.type = b2_dynamicBody;
	b2Body* Body = World.CreateBody(&BodyDef);
	BodyDef.allowSleep = false;
	BodyDef.bullet = true;
	BodyDef.active = true;
	BodyDef.awake = true;

	// Define another box shape for our dynamic body.
	b2CircleShape circleShape;
	circleShape.m_radius = radius / scale;
	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circleShape;
	// Set the box density to be non-zero, so it will be dynamic.
	fixtureDef.density = .01f;
	// Override the default friction.
	fixtureDef.friction = 0.0f;
	fixtureDef.restitution = .9f;
	// Add the shape to the body.
	Body->CreateFixture(&fixtureDef);

}

void DrawObjects::CreateWorldBox(b2World &World, float heigth, float width, float scale)
{
	float widthInMeters = width / scale;
	float heightInMeters = heigth / scale;
	b2Vec2 lowerLeftCorner = b2Vec2(0, 0);
	b2Vec2 lowerRightCorner = b2Vec2(widthInMeters, 0);
	b2Vec2 upperLeftCorner = b2Vec2(0, heightInMeters);
	b2Vec2 upperRightCorner = b2Vec2(widthInMeters, heightInMeters);

	// static container body, with the collisions at screen borders
	b2BodyDef screenBorderDef;
	screenBorderDef.position.Set(0, 0);
	b2Body* screenBorderBody = World.CreateBody(&screenBorderDef);
	b2EdgeShape screenBorderShape;

	// Create fixtures for the four borders (the border shape is re-used)
	screenBorderShape.Set(lowerLeftCorner, lowerRightCorner);
	screenBorderBody->CreateFixture(&screenBorderShape, 0);
	screenBorderShape.Set(lowerRightCorner, upperRightCorner);
	screenBorderBody->CreateFixture(&screenBorderShape, 0);
	screenBorderShape.Set(upperRightCorner, upperLeftCorner);
	screenBorderBody->CreateFixture(&screenBorderShape, 0);
	screenBorderShape.Set(upperLeftCorner, lowerLeftCorner);
	screenBorderBody->CreateFixture(&screenBorderShape, 0);
}
