#pragma once
#include <Box2D/Box2D.h>
#include <SFML\Graphics.hpp>
#include <iostream>


class DrawObjects
{
public:
	DrawObjects();
	~DrawObjects();
	
	void CreateLine(b2World & World, int MouseX, int MouseY, float heigth, float width, float scale);
	void CreateBox(b2World &World, int MouseX, int MouseY, float heigth, float width, float scale);
	void CreateCircle(b2World &World, float radius, int MouseX, int MouseY, float scale);
	void CreateWorldBox(b2World &World, float heigth, float width, float scale);
	void CreateGround(b2World & World, float X, float Y);
};

