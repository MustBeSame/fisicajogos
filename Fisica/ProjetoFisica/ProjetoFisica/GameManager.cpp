#include "GameManager.h"

const float pixel_scale = 30.f;

GameManager::GameManager()
{
}


GameManager::~GameManager()
{
}

void GameManager::CreateScene1(b2World &World, sf::RenderWindow &Window)
{
	physics.CreateWorldBox(World, Window.getSize().x, Window.getSize().y, pixel_scale);

	physics.CreateCircle(World, 20.f, 200, 30, pixel_scale);

	physics.CreateLine(World, 200, 500, 8.f, 64.f, pixel_scale);

}

