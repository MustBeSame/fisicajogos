#include <SFML\Graphics.hpp>
#include <Box2D\Box2D.h>
#include "DrawObjects.h"
#include "GameManager.h"

#include <iostream>
#include <ctime>


using namespace sf;
using namespace std;

Event event;
DrawObjects drawObjects;
GameManager gm;
bool gravity = false;
const float pixel_scale = 30.f;
float gravityForce = 80.f;


int main() {

	sf::RenderWindow window(sf::VideoMode(640, 480, 32), "SFML TUTORIAL");
	b2Vec2 gravityVector(0.f, gravityForce);

	b2World m_world(gravityVector);
	window.setFramerateLimit(60);
	Texture groundTexture;

	gm.state(NotStarted);

	//Metodo que criador da fase

	//drawObjects.CreateWorldBox(m_world, window.getSize().y, window.getSize().x, pixel_scale);

	while (window.isOpen()) {
		while (window.pollEvent(event)) {
			if (event.type == Event::KeyReleased && event.key.code == Keyboard::Escape)
			{
				window.close();
			}

			if (gm.state() != Running) {
				gm.CreateScene1(m_world, window);
				gm.state(Running);
			}

			if (event.type == Event::KeyReleased && event.key.code == Keyboard::G)
			{
				gravity = !gravity;
				m_world.SetGravity(gravity ? b2Vec2(0.f, -gravityForce) : b2Vec2(0.f, gravityForce));
			}
		}
		m_world.Step(1 / 120.f, 8, 3);
		window.clear(sf::Color(126, 60, 224));

		for (b2Body* BodyIterator = m_world.GetBodyList(); BodyIterator != 0; BodyIterator = BodyIterator->GetNext())
		{
			if (BodyIterator->GetType() == b2_dynamicBody)
			{
				for (b2Fixture* FixtureIterator = BodyIterator->GetFixtureList(); FixtureIterator != 0; FixtureIterator = FixtureIterator->GetNext())
				{
					if (FixtureIterator->GetShape()->GetType() == b2Shape::e_circle)
					{
						CircleShape circleS;
						circleS.setFillColor(Color(255, 255, 0));
						circleS.setRadius(20.f);
						circleS.setPosition(Vector2f(pixel_scale * BodyIterator->GetPosition().x, pixel_scale *  BodyIterator->GetPosition().y));
						circleS.setOrigin(Vector2f(circleS.getGlobalBounds().width / 2.f, circleS.getGlobalBounds().height / 2.f));
						window.draw(circleS);
					}
				}
			} else
			{
				RectangleShape lineS;
				lineS.setFillColor(Color(200, 134, 80));
				lineS.setPosition(Vector2f(pixel_scale * BodyIterator->GetPosition().x, pixel_scale * BodyIterator->GetPosition().y));
				lineS.setSize(Vector2f(64.f, 8.f));

				lineS.setOrigin(4.f, 32.f);
				lineS.setRotation(BodyIterator->GetAngle() * 180 / b2_pi);
				window.draw(lineS);
			}
		}
		window.display();
	}
	return 0;
	//while (window.isOpen()) {
	//	while (window.pollEvent(event))
	//	{
	//		if (event.type == Event::KeyReleased && event.key.code == Keyboard::Escape)
	//		{
	//			window.close();
	//		}

	//		if (event.type == Event::KeyReleased && event.key.code == Keyboard::C)
	//			actualShape = circle;
	//		if (event.type == Event::KeyReleased && event.key.code == Keyboard::B)
	//			actualShape = box;
	//		if (event.type == Event::KeyReleased && event.key.code == Keyboard::L)
	//			actualShape = line;

	//		if (Mouse::isButtonPressed(Mouse::Left))
	//		{
	//			int MouseX = Mouse::getPosition(window).x;
	//			int MouseY = Mouse::getPosition(window).y;

	//			if (actualShape == circle)
	//				drawObjects.CreateCircle(m_world, 30.f, MouseX, MouseY, pixel_scale);
	//			if (actualShape == box)
	//				drawObjects.CreateBox(m_world, MouseX, MouseY, 32.f, 32.f, pixel_scale);
	//			if(actualShape == line)
	//				drawObjects.CreateLine(m_world, MouseX, MouseY, 8.f, 64.f, pixel_scale);
	//		}

	//		//Vetor de Gravidade Cima e Baixo sendo alterado pela tecla G
	//		if (event.type == Event::KeyReleased && event.key.code == Keyboard::G)
	//		{
	//			gravity = !gravity;
	//			m_world.SetGravity(gravity ? b2Vec2(0.f, -10.f) : b2Vec2(0.f, 10.f));
	//		}
	//		//Vetor de Gravidade Esq Dir sendo alterado pela tecla G
	//		if (event.type == Event::KeyReleased && event.key.code == Keyboard::H)
	//		{
	//			gravity = !gravity;
	//			m_world.SetGravity(gravity ? b2Vec2(-10.f, 0.f) : b2Vec2(10.f, 0.f));
	//		}
	//		//Zera o a gravidade
	//		if (event.type == Event::KeyReleased && event.key.code == Keyboard::Z)
	//		{
	//			m_world.SetGravity(b2Vec2(0.f, 0.f));
	//		}
	//	}
	//	m_world.Step(1 / 60.f, 8, 3);
	//	window.clear(sf::Color::White);
	//	//Pegar ao inves dos bodys, percorrer tb as Fixtures, e desenhar as formas a partir das fixtures.
	//	for (b2Body* BodyIterator = m_world.GetBodyList(); BodyIterator != 0; BodyIterator = BodyIterator->GetNext())
	//	{
	//		if (BodyIterator->GetType() == b2_dynamicBody)
	//		{
	//			for (b2Fixture* FixtureIterator = BodyIterator->GetFixtureList(); FixtureIterator != 0; FixtureIterator = FixtureIterator->GetNext())
	//			{
	//				if (FixtureIterator->GetShape()->GetType() == b2Shape::e_circle)
	//				{
	//					CircleShape circleS;
	//					circleS.setFillColor(Color(255, 255, 0));
	//					circleS.setRadius(30.f);
	//					circleS.setPosition(Vector2f(pixel_scale * BodyIterator->GetPosition().x, pixel_scale *  BodyIterator->GetPosition().y));
	//					circleS.setOrigin(Vector2f(circleS.getGlobalBounds().width / 2.f, circleS.getGlobalBounds().height / 2.f));
	//					window.draw(circleS);
	//				}
	//				else if (FixtureIterator->GetShape()->GetType() == b2Shape::e_polygon && BodyIterator->GetUserData() != "Line")
	//				{
	//					RectangleShape boxS;
	//					boxS.setFillColor(Color(0, 255, 0));
	//					boxS.setPosition(Vector2f(pixel_scale * BodyIterator->GetPosition().x, pixel_scale * BodyIterator->GetPosition().y));
	//					boxS.setSize(Vector2f(32.f, 32.f));
	//					boxS.setOrigin(16.f, 16.f);
	//					boxS.setRotation(BodyIterator->GetAngle() * 180 / b2_pi);
	//					window.draw(boxS);
	//				}
	//				else if (FixtureIterator->GetShape()->GetType() == b2Shape::e_polygon && BodyIterator->GetUserData() == "Line")
	//				{
	//					RectangleShape lineS;
	//					lineS.setFillColor(Color(200, 134, 80));
	//					lineS.setPosition(Vector2f(pixel_scale * BodyIterator->GetPosition().x, pixel_scale * BodyIterator->GetPosition().y));
	//					lineS.setSize(Vector2f(64.f, 8.f));

	//					lineS.setOrigin(4.f, 32.f);
	//					lineS.setRotation(BodyIterator->GetAngle() * 180 / b2_pi);
	//					window.draw(lineS);
	//				}
	//			}
	//		}
	//		else {
	//			Sprite GroundSprite;
	//			GroundSprite.setTexture(groundTexture);
	//			GroundSprite.setOrigin(400.f, 8.f);
	//			GroundSprite.setPosition(BodyIterator->GetPosition().x * 30, BodyIterator->GetPosition().y * 30);
	//			GroundSprite.setRotation(180 / b2_pi * BodyIterator->GetAngle());
	//			window.draw(GroundSprite);
	//		}
	//	}

	//	window.display();
	//}

}
