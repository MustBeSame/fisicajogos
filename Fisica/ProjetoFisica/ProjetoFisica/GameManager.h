#pragma once
#include <Box2D/Box2D.h>
#include <SFML\Graphics.hpp>
#include <iostream>
#include "DrawObjects.h"

enum GameStates { Menu, Paused, Running, NotStarted };
class GameManager
{
private:
	GameStates states;
	DrawObjects physics;
public:
	GameManager();
	~GameManager();
	
	int state() const { return states; }
	void state(GameStates v) { states = v; }

	void CreateScene1(b2World &World, sf::RenderWindow &Window);
	
};

